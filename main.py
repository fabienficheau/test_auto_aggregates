import itertools
from collections import defaultdict

seq0 =         ["mod1", "mod2", "mod3", "mod4",       "modC1", "modC2"]
seq1 = ["mod0", "mod1", "mod2", "mod3"]
seq2 =         ["mod1", "mod2", "mod3",        "mod5","modC1", "modC2" ]
seq3 = ["moda", "modb", "modc", "mod1", "truc0", "truc1", "truc2", "truc3"]
# seq4 = ["mod1", "moda", "modb", "modc"]
seq4 = ["mod0", "mod1", "moda", "modd", "modb", "modc", "truc0", "truc1", "truc2", "truc3"]
seq5 =                [ "moda", "modd", "modb", "modA"]
seq6 =          ["mod1", "mod2", "modJ"]
seq7 =          ["mod1", "mod2", "modK", "truc0", "truc1", "truc2", "truc3"]

# genseqname_to_genseq_map = {}
# genseq_to_genseqname_map = {}
# genseqname_offset = 0

# decomposed_seqs = {}

def push_genseq(seq, genseq):
    # genseqname_offset = genseq['genseqname_offset']
    # genseqname_to_genseq_map = genseq['genseqname_to_genseq_map']
    # genseq_to_genseqname_map = genseq['genseq_to_genseqname_map']

    tupled_seq = tuple(seq)
    if tupled_seq in genseq['genseq_to_genseqname_map']:
        return genseq['genseq_to_genseqname_map'][tupled_seq]
    letteredint = 'gen_'+str(genseq['prefix_discrim'])+'_'+str(genseq['genseqname_offset'])
    genseq['genseqname_to_genseq_map'][letteredint]=tupled_seq
    genseq['genseq_to_genseqname_map'][tupled_seq]=letteredint
    toreturn_seqname = letteredint
    genseq['genseqname_offset'] = genseq['genseqname_offset']+1
    return toreturn_seqname

def decompose_seq(seq_name, seqname_to_seq_map, seq_intersection, genseq, decomposed_seqs):
    # print "decompose_seq, seq_name: " + str(seq_name) + ", seqname_to_seq_map: " + str(seqname_to_seq_map) + ", "
    if seq_name in decomposed_seqs:
        return
    # print "***decompose_seq start"
    # print "seq_name: " + seq_name
    # print "seq_intersection: " + str(seq_intersection)
    # print "seq_contents: " + str(seqname_to_seq_map[seq_name])
    decomposed_sum = tuple()
    seq = seqname_to_seq_map[seq_name]
    seq_difference = set(seq).difference(seq_intersection)
    # print "seq_difference: " + str(seq_difference)
    # print "decompose_seq end***"
    genseqname = push_genseq(seq_intersection, genseq)
    decomposed_seqs[seq_name]=(genseqname,seq_difference)
    # print "decomposed_seqs: " +str(decomposed_seqs)

def generate_list():
    seq_list = [seq0, seq1, seq2, seq3, seq4, seq5, seq6, seq7]
    seqname_list = ['seq0', 'seq1', 'seq2', 'seq3', 'seq4', 'seq5', 'seq6', 'seq7']

    to_return_seq = []
    seq_to_seqname_map = {}
    seqname_to_seq_map = {}
    for idx, seq in enumerate(seq_list):
        sorted_seq = sorted(seq)
        seq_to_seqname_map[tuple(sorted_seq)] = seqname_list[idx]
        seqname_to_seq_map[seqname_list[idx]] = tuple(sorted_seq)
        to_return_seq.append(sorted_seq)

    # to_return_seq = []
    # seq_to_seqname_map = {}
    # seqname_to_seq_map = {}
    # to_return_seq.append(sorted(seq0))
    # seq_to_seqname_map[tuple(to_return_seq[-1])]='seq0'
    # seqname_to_seq_map['seq0']=tuple(to_return_seq[-1])
    # to_return_seq.append(sorted(seq1))
    # seq_to_seqname_map[tuple(to_return_seq[-1])] = 'seq1'
    # seqname_to_seq_map['seq1'] = tuple(to_return_seq[-1])
    # to_return_seq.append(sorted(seq2))
    # seq_to_seqname_map[tuple(to_return_seq[-1])] = 'seq2'
    # seqname_to_seq_map['seq2'] = tuple(to_return_seq[-1])
    # to_return_seq.append(sorted(seq3))
    # seq_to_seqname_map[tuple(to_return_seq[-1])] = 'seq3'
    # seqname_to_seq_map['seq3'] = tuple(to_return_seq[-1])
    # to_return_seq.append(sorted(seq4))
    # seq_to_seqname_map[tuple(to_return_seq[-1])] = 'seq4'
    # seqname_to_seq_map['seq4'] = tuple(to_return_seq[-1])
    # to_return_seq.append(sorted(seq5))
    # seq_to_seqname_map[tuple(to_return_seq[-1])] = 'seq5'
    # seqname_to_seq_map['seq5'] = tuple(to_return_seq[-1])
    # to_return_seq.append(sorted(seq6))
    # seq_to_seqname_map[tuple(to_return_seq[-1])] = 'seq6'
    # seqname_to_seq_map['seq6'] = tuple(to_return_seq[-1])
    # to_return_seq.append(sorted(seq7))
    # seq_to_seqname_map[tuple(to_return_seq[-1])] = 'seq7'
    # seqname_to_seq_map['seq7'] = tuple(to_return_seq[-1])
    # return to_return_seq, seq_to_seqname_map, seqname_to_seq_map
    return to_return_seq, seq_to_seqname_map, seqname_to_seq_map

def generate_workseq(seq_to_seqname_map):
    toreturn_workseq = []
    for seq in seq_to_seqname_map.iterkeys():
        toreturn_workseq.append(seq)
    return toreturn_workseq

# myiter = 0;

def generate_decomposed_seqs(to_return_seq, seq_to_seqname_map, seqname_to_seq_map, results, recursve_level = 0):
    if len(to_return_seq)==0:
        results[recursve_level] = {}
        return

    largest_aggregates = 0
    largest_intersactions = defaultdict(list)
    decomposed_seqs = {}

    genseq = {}
    genseq['genseqname_to_genseq_map'] = {}
    genseq['genseq_to_genseqname_map'] = {}
    genseq['genseqname_offset'] = 0
    genseq['prefix_discrim'] = recursve_level

    already_intersected = defaultdict(int)
    for nwise_comp_order in range(len(to_return_seq), 1, -1):
        print ">>>>>>>>>>>>>>>>>>>>>>>>>>>nwise_comp_order: " + str(nwise_comp_order)
        for nwise_combination in itertools.combinations(to_return_seq, nwise_comp_order):
            # print nwise_combination
            intersection_set = set.intersection(*map(set, nwise_combination))
            # print "intersection_set: " + str(intersection_set)
            # print "size : " + str(len(intersection_set))
            current_intersection_set_len = len(intersection_set)
            idx_seq0 = seq_to_seqname_map[tuple(nwise_combination[0])]
            idx_seq1 = seq_to_seqname_map[tuple(nwise_combination[1])]
            intersected_seqs_key = tuple([idx_seq0, idx_seq1])
            if current_intersection_set_len > 1:
                # largest_intersactions[current_intersection_set_len].append((str( idx_seq0 + " <=> " + idx_seq1),intersection_set))
                if intersected_seqs_key in largest_intersactions[current_intersection_set_len]:
                    print "Case to check!!!!: " + str(intersected_seqs_key)
                else:
                    largest_intersactions[current_intersection_set_len].append((intersected_seqs_key, intersection_set))
                    if (current_intersection_set_len > largest_aggregates):
                        largest_aggregates = current_intersection_set_len
                        print ">>>New largest set, with size: " + str(current_intersection_set_len)

    for intersection_size in reversed(sorted(largest_intersactions)):
        print intersection_size
        print "Biggest cluster is " + str(intersection_size)
        values = largest_intersactions[intersection_size]
        print "Values: " + str(values)
        for value in values:
            compared_seqs = value[0]
            intersect_seq = value[1]
            decompose_seq(compared_seqs[0], seqname_to_seq_map, intersect_seq, genseq, decomposed_seqs)
            decompose_seq(compared_seqs[1], seqname_to_seq_map, intersect_seq, genseq, decomposed_seqs)

    print "decomposed_seqs 1" + str(decomposed_seqs)
    print "genseqname_to_genseq_map 1" + str(genseq['genseqname_to_genseq_map'])

    results[recursve_level] = {}
    results[recursve_level]['decomposed_seqs'] = decomposed_seqs
    results[recursve_level]['genseqname_to_genseq_map'] = genseq['genseqname_to_genseq_map']
    recursve_level = recursve_level + 1
    generate_decomposed_seqs(generate_workseq(genseq['genseq_to_genseqname_map']),
                                genseq['genseq_to_genseqname_map'],
                                genseq['genseqname_to_genseq_map'],
                                results,
                                recursve_level)
    return

if __name__ == '__main__':
    to_return_seq, seq_to_seqname_map, seqname_to_seq_map = generate_list()
    results = {}
    # print "aftersortseq_to_seqname_map: " + str(to_return_seq)
    generate_decomposed_seqs(to_return_seq,seq_to_seqname_map,seqname_to_seq_map, results)
    print "results: " + str(results)
